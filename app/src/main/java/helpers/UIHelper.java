package helpers;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.widget.EditText;

import java.util.concurrent.Callable;


public class UIHelper {
    private static String dialogResult = "";

    public static String getDialogResult() {
        return dialogResult;
    }

    public static void createDialog(Context context, final Callable positiveAction) {
        UIHelper.createDialog(context, "Save to file", "Masukan Nama File", positiveAction);
    }

    public static void createDialog(Context context, String title, String message, final Callable<String> positiveAction) {
        AlertDialog.Builder alert = new AlertDialog.Builder(context);

        alert.setTitle(title);
        alert.setMessage(message);

// Set an EditText view to get user input
        final EditText input = new EditText(context);
        alert.setView(input);

        alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                UIHelper.dialogResult = input.getText().toString();

                try {
                    positiveAction.call();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                // Canceled.
            }
        });
        alert.show();
    }
}
