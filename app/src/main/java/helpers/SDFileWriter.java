package helpers;

import android.os.Environment;

import java.io.File;
import java.io.IOException;

@SuppressWarnings("ALL")
public class SDFileWriter {
    private static final String fileBasePath = Environment.getExternalStorageDirectory().getPath();

    public static void WriteToFile(String filename, String message) {
        try {
            String fileTarget = fileBasePath + "/" + filename;
            File file = new File(fileTarget);
            if (!file.exists()) file.createNewFile();

            java.io.FileWriter filewriter = new java.io.FileWriter(fileTarget, true);
            filewriter.write(message + "\n");
            filewriter.close();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
    }
}
