package com.example.asihnopriadi.apk1;

import android.app.ActionBar;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

@SuppressWarnings("ALL")
public class map_point extends FragmentActivity {

    EditText etLng;
    EditText etLat;
    GoogleMap googleMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.map_point);
        ActionBar actionBar = getActionBar();
        actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#3b5998")));
        assert actionBar != null;
        actionBar.setHomeButtonEnabled(true);
        getActionBar().setDisplayHomeAsUpEnabled(true);

        // Reference to the EditText et_lat of the layout activity_maps.xml
        etLat = (EditText)findViewById(R.id.et_lat);

        // Reference to the EditText et_lng of the layout activity_maps.xml
        etLng = (EditText)findViewById(R.id.et_lng);

        etLng.setOnEditorActionListener(new TextView.OnEditorActionListener() {

            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if(actionId == EditorInfo.IME_ACTION_DONE){

                    // Getting the entered latitude
                    String lat = etLat.getText().toString();

                    // Getting the entered longitude
                    String lng = etLng.getText().toString();

                    // Getting Google Play availability status
                    int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getBaseContext());

                    // Showing status
                    if(status!= ConnectionResult.SUCCESS){ // Google Play Services are not available

                        int requestCode = 10;
                        Dialog dialog = GooglePlayServicesUtil.getErrorDialog(status, getParent(), requestCode);
                        dialog.show();

                    }else { // Google Play Services are available

                        // Getting reference to the SupportMapFragment of activity_main.xml
                        SupportMapFragment fm = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map_point);


                        // Getting GoogleMap object from the fragment
                        //googleMap = fm.getMap();

                        // Enabling MyLocation Layer of Google Map
                        googleMap.setMyLocationEnabled(true);

                        // set type Map
                        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

                        // LatLng object to store user input coordinates
                        LatLng point = new LatLng(Double.parseDouble(lat), Double.parseDouble(lng));

                        // Drawing the marker at the coordinates
                        drawMarker(point);
                    }
                }
                return false;
            }
        });
    }

    private void drawMarker(LatLng point){
        // Clears all the existing coordinates
        googleMap.clear();

        // Creating an instance of MarkerOptions
        MarkerOptions markerOptions = new MarkerOptions();

        // Setting latitude and longitude for the marker
        markerOptions.position(point);

        // Setting title for the InfoWindow
        markerOptions.title("Posisi");

        // Setting InfoWindow contents
        markerOptions.snippet("X : " + point.latitude + " ; " + "Y : " + point.longitude);

        // Adding marker on the Google Map
        googleMap.addMarker(markerOptions);

        // Moving CameraPosition to the user input coordinates
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(point));

    }

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_koordinat, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        menu.findItem(R.id.map_point).setChecked(true);
        return true;
    }

  /*  public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                Intent intent = new Intent(this, MapActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                return true;
            case R.id.map_line:
                intent = new Intent(this, map_line.class);
                startActivity(intent);
                return true;
            case R.id.map_polygon:
                intent = new Intent(this, map_polygon.class);
                startActivity(intent);
                return true;
            case R.id.map_base_point:
                intent = new Intent(this, map_base_point.class);
                startActivity(intent);
                return (true);
            case R.id.clear:
                intent = new Intent(this, map_point.class);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }*/
}

