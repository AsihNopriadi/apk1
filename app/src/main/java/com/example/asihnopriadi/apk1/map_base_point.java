package com.example.asihnopriadi.apk1;

import android.app.ActionBar;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.concurrent.Callable;

import helpers.SDFileWriter;
import helpers.UIHelper;

@SuppressWarnings("ALL")
public class   map_base_point extends FragmentActivity {

    ImageButton imageButton;
    LatLng myPosition;
    SharedPreferences sharedPreferences;
    int locationCount = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.map_base_point);
        ActionBar actionBar = getActionBar();
        actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#3b5998")));
        assert actionBar != null;
        actionBar.setHomeButtonEnabled(true);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        addListenerOnButton();

        // Getting Google Play availability status
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getBaseContext());

        // Showing status
        if (status != ConnectionResult.SUCCESS) { // Google Play Services are not available

            int requestCode = 5;
            Dialog dialog = GooglePlayServicesUtil.getErrorDialog(status, this, requestCode);
            dialog.show();

        } else { // Google Play Services are available

            // Getting reference to the SupportMapFragment of activity_main.xml
            SupportMapFragment fm = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map_base_point);
            // Getting GoogleMap object from the fragment
            //googleMap = fm.getMap();

            // Enabling MyLocation Layer of Google Map
            googleMap.setMyLocationEnabled(true);

            //set auto current location
            LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
            Criteria criteria = new Criteria();
            String provider = locationManager.getBestProvider(criteria, true);
            Location location = locationManager.getLastKnownLocation(provider);

            //auto zoom current location and info coordinat
            if (location != null) {
                TextView tvLocation = (TextView) findViewById(R.id.tv_location);
                double latitude = location.getLatitude();
                double longitude = location.getLongitude();
                LatLng latLng = new LatLng(latitude, longitude);
                myPosition = new LatLng(latitude, longitude);
                LatLng coordinate = new LatLng(latitude, longitude);
                CameraUpdate yourLocation = CameraUpdateFactory.newLatLngZoom(coordinate, 15);
                googleMap.animateCamera(yourLocation);
                // Setting latitude and longitude in the TextView tv_location
                tvLocation.setText("X : " + String.format("%.4f", latitude) + " ; "
                        + "Y : " + String.format("%.4f", longitude));
            }

            // Opening the sharedPreferences object
            sharedPreferences = getSharedPreferences("location", 0);

            // Getting number of locations already stored
            locationCount = sharedPreferences.getInt("locationCount", 0);

            // Getting stored zoom level if exists else return 0
            String zoom = sharedPreferences.getString("zoom", "0");

            // If locations are already saved
            if (locationCount != 0) {

                String lat = "";
                String lng = "";

                // Iterating through all the locations stored
                for (int i = 0; i < locationCount; i++) {

                    // Getting the latitude of the i-th location
                    lat = sharedPreferences.getString("lat" + i, "0");

                    // Getting the longitude of the i-th location
                    lng = sharedPreferences.getString("lng" + i, "0");

                    // Drawing marker on the map
                    drawMarker(new LatLng(Double.parseDouble(lat), Double.parseDouble(lng)));
                }

                // Moving CameraPosition to last clicked position
                googleMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(Double.parseDouble(lat), Double.parseDouble(lng))));

                // Setting the zoom level in the map on last position  is clicked
                googleMap.animateCamera(CameraUpdateFactory.zoomTo(Float.parseFloat(zoom)));
            }
        }

        googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {

            @Override
            public void onMapClick(LatLng point) {
                locationCount++;
                drawMarker(point);
                Toast.makeText(getBaseContext(), "Marker berhasil ditambahkan", Toast.LENGTH_SHORT).show();

            }
        });


        googleMap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
            @Override
            public void onMapLongClick(LatLng point) {

                // Removing the marker and circle from the Google Map
                googleMap.clear();

                // Opening the editor object to delete data from sharedPreferences
                SharedPreferences.Editor editor;
                editor = sharedPreferences.edit();

                // Clearing the editor
                editor.clear();

                // Committing the changes
                editor.apply();

                // Setting locationCount to zero
                locationCount = 0;

            }
        });
    }

    GoogleMap googleMap;

    ArrayList<LatLng> latlngCollection = null;

    private void drawMarker(LatLng point) {
        if (latlngCollection == null)
            latlngCollection = new ArrayList<LatLng>();

        latlngCollection.add(point);
        redrawMap();
    }

    private void redrawMap() {
        // Clears all the existing coordinates
        googleMap.clear();

        if (latlngCollection.size() > 0) {
            for (LatLng curPoint : latlngCollection) {
                // Creating an instance of MarkerOptions
                MarkerOptions markerOptions = new MarkerOptions();

                markerOptions.title("Posisi");

                // Setting InfoWindow contents
                markerOptions.snippet("X : " + String.format("%.2f", curPoint.latitude) + " ; " + "Y : " + String.format("%.2f", curPoint.longitude));

                // Setting latitude and longitude for the marker
                markerOptions.position(curPoint);

                TextView tvLocation = (TextView) findViewById(R.id.tv_location);

                // Setting latitude and longitude in the TextView tv_location
                tvLocation.setText("X : " + String.format("%.4f", curPoint.latitude) + " ; " + "Y : " + String.format("%.4f", curPoint.longitude));

                // Adding marker on the Google Map
                googleMap.addMarker(markerOptions);
            }

            LatLng point = latlngCollection.get(latlngCollection.size() - 1);

            // Moving CameraPosition to the user input coordinates
            googleMap.moveCamera(CameraUpdateFactory.newLatLng(point));
        }
    }

    public void addListenerOnButton() {

        imageButton = (ImageButton) findViewById(R.id.btn_undo);

        imageButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                if (latlngCollection.size() > 0) {
                    latlngCollection.remove(latlngCollection.size() - 1);
                    redrawMap();
                    Toast.makeText(getBaseContext(), "Undo berhasil", Toast.LENGTH_SHORT).show();
                }
            }

        });

        ImageButton saveButton = (ImageButton) findViewById(R.id.btn_save);
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Callable c = new Callable() {
                    @Override
                    public Object call() throws Exception {
                        String output = "Kode Longitude Latitude\n";
                        int i = 1;
                        for(LatLng l : map_base_point.this.latlngCollection) {
                            output += "P" + i++;
                            output += " = " + String.valueOf(l.longitude);
                            output += " = " + String.valueOf(l.latitude);
                            output += "\n";
                        }


                        SDFileWriter.WriteToFile(UIHelper.getDialogResult(), output);
                        return null;
                    }
                };

                UIHelper.createDialog(map_base_point.this, c);
            }
        });

    }
/*
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        menu.findItem(R.id.map_base_point).setChecked(true);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                Intent intent = new Intent(this, MapActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                return true;
            case R.id.terrain:
                googleMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
                return true;
            case R.id.hybrid:
                googleMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
                return true;
            case R.id.normal:
                googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                return true;
            case R.id.map_line:
                intent = new Intent(this, map_line.class);
                startActivity(intent);
                return true;
            case R.id.map_polygon:
                intent = new Intent(this, map_polygon.class);
                startActivity(intent);
                return true;
            case R.id.map_point:
                intent = new Intent(this, map_point.class);
                startActivity(intent);
                return (true);
            case R.id.clear:
                latlngCollection.clear();
                redrawMap();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }*/
}

